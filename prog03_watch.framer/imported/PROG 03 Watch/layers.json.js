window.__imported__ = window.__imported__ || {};
window.__imported__["PROG 03 Watch/layers.json.js"] = [
  {
    "maskFrame" : null,
    "id" : "198EAD1A-50E2-4CEC-ADFA-61960776B2BA",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Final_Destination-198EAD1A-50E2-4CEC-ADFA-61960776B2BA.png",
      "frame" : {
        "y" : 0,
        "x" : 1121,
        "width" : 320,
        "height" : 400
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 1121,
      "width" : 320,
      "height" : 400
    },
    "name" : "Final_Destination"
  },
  {
    "maskFrame" : null,
    "id" : "315701AF-6313-423D-943A-0425F0339FDB",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Destination_Ahead-315701AF-6313-423D-943A-0425F0339FDB.png",
      "frame" : {
        "y" : 0,
        "x" : 767,
        "width" : 320,
        "height" : 400
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 767,
      "width" : 320,
      "height" : 400
    },
    "name" : "Destination_Ahead"
  },
  {
    "maskFrame" : null,
    "id" : "C1D9DE05-1FAD-4C80-8974-4EFFEB8B7C0A",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Direction-C1D9DE05-1FAD-4C80-8974-4EFFEB8B7C0A.png",
      "frame" : {
        "y" : 0,
        "x" : 379,
        "width" : 320,
        "height" : 400
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 379,
      "width" : 320,
      "height" : 400
    },
    "name" : "Direction"
  },
  {
    "maskFrame" : null,
    "id" : "0F5A96CB-E98B-4CE3-BA77-2F98B7D4BB90",
    "visible" : true,
    "children" : [
      {
        "maskFrame" : null,
        "id" : "783E4452-805E-4B9C-9631-D5D3D98FA395",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/Accept_Button-783E4452-805E-4B9C-9631-D5D3D98FA395.png",
          "frame" : {
            "y" : 247,
            "x" : 22,
            "width" : 279,
            "height" : 65
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 247,
          "x" : 22,
          "width" : 279,
          "height" : 65
        },
        "name" : "Accept_Button"
      }
    ],
    "image" : {
      "path" : "images\/Invitation-0F5A96CB-E98B-4CE3-BA77-2F98B7D4BB90.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 320,
        "height" : 400
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 320,
      "height" : 400
    },
    "name" : "Invitation"
  }
]