// This is autogenerated by Framer Studio


// Generated by CoffeeScript 1.8.0
(function() {
  var lookupLine, properties, _RESULT,
    __slice = [].slice;

  if (window.FramerStudio == null) {
    window.FramerStudio = {};
  }

  window.onerror = null;

  window.midiCommand = window.midiCommand || function() {};

  if (Framer.Device) {
    properties = ["deviceScale", "contentScale", "deviceType", "keyboard", "orientation", "fullScreen"];
    properties.map(function(propertyName) {
      return Framer.Device.on("change:" + propertyName, function() {
        return window._bridge("device:change");
      });
    });
  }

  _RESULT = null;

  lookupLine = function(lineNumber) {
    var char, charIndex, errorColNumber, errorLine, errorLineIndex, errorLineNumber, loc, sourceLines, _i, _len;
    sourceLines = _RESULT.js.split("\n");
    errorLineIndex = lineNumber - 1;
    errorLine = sourceLines[errorLineIndex];
    if (!errorLine) {
      return lineNumber;
    }
    errorLineNumber = 1;
    errorColNumber = 0;
    for (charIndex = _i = 0, _len = errorLine.length; _i < _len; charIndex = ++_i) {
      char = errorLine[charIndex];
      loc = _RESULT.sourceMap.sourceLocation([errorLineIndex, charIndex]);
      if (loc && loc[0] > errorLineNumber) {
        errorLineNumber = loc[0] + 1;
        errorColNumber = loc[1];
      }
    }
    console.log("lineNumber", lineNumber);
    console.log("errorLineNumber", errorLineNumber);
    return errorLineNumber;
  };

  FramerStudio.compile = function(code) {
    var e, err, errorMessage;
    console.log("FramerStudio.compile");
    window.onerror = null;
    window.onresize = null;
    try {
      _RESULT = CoffeeScript.compile(code, {
        sourceMap: true,
        filename: "generated.js"
      });
    } catch (_error) {
      e = _error;
      console.log("Compile error:", e);
      if (e instanceof SyntaxError) {
        errorMessage = e.stack;
        err = new SyntaxError(e.message);
        err.line = e.location.first_line;
        err.lineNumber = e.location.first_line;
        err.lookup = true;
        window._bridge("StudioError", {
          message: e.message,
          line: e.location.first_line,
          lineNumber: e.location.first_line,
          errorType: "compile"
        });
        throw err;
      } else {
        throw e;
      }
    }
    window.onerror = function(errorMsg, url, lineNumber) {
      var error;
      console.log.apply(console, ["Eval error:"].concat(__slice.call(arguments)));
      error = new Error(errorMsg);
      error.line = lookupLine(lineNumber);
      window._bridge("StudioError", {
        message: errorMsg,
        line: error.line,
        lineNumber: error.line,
        errorType: "eval"
      });
      throw error;
    };
    return _RESULT.js;
  };

  if (typeof window._bridge === "function") {
    window._bridge("StudioScriptLoaded");
  }

}).call(this);

window.__imported__ = window.__imported__ || {};
window.__imported__["PROG 03 Watch/layers.json.js"] = [
  {
    "maskFrame" : null,
    "id" : "198EAD1A-50E2-4CEC-ADFA-61960776B2BA",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Final_Destination-198EAD1A-50E2-4CEC-ADFA-61960776B2BA.png",
      "frame" : {
        "y" : 0,
        "x" : 1121,
        "width" : 320,
        "height" : 400
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 1121,
      "width" : 320,
      "height" : 400
    },
    "name" : "Final_Destination"
  },
  {
    "maskFrame" : null,
    "id" : "315701AF-6313-423D-943A-0425F0339FDB",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Destination_Ahead-315701AF-6313-423D-943A-0425F0339FDB.png",
      "frame" : {
        "y" : 0,
        "x" : 767,
        "width" : 320,
        "height" : 400
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 767,
      "width" : 320,
      "height" : 400
    },
    "name" : "Destination_Ahead"
  },
  {
    "maskFrame" : null,
    "id" : "C1D9DE05-1FAD-4C80-8974-4EFFEB8B7C0A",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Direction-C1D9DE05-1FAD-4C80-8974-4EFFEB8B7C0A.png",
      "frame" : {
        "y" : 0,
        "x" : 379,
        "width" : 320,
        "height" : 400
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 379,
      "width" : 320,
      "height" : 400
    },
    "name" : "Direction"
  },
  {
    "maskFrame" : null,
    "id" : "0F5A96CB-E98B-4CE3-BA77-2F98B7D4BB90",
    "visible" : true,
    "children" : [
      {
        "maskFrame" : null,
        "id" : "783E4452-805E-4B9C-9631-D5D3D98FA395",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/Accept_Button-783E4452-805E-4B9C-9631-D5D3D98FA395.png",
          "frame" : {
            "y" : 247,
            "x" : 22,
            "width" : 279,
            "height" : 65
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 247,
          "x" : 22,
          "width" : 279,
          "height" : 65
        },
        "name" : "Accept_Button"
      }
    ],
    "image" : {
      "path" : "images\/Invitation-0F5A96CB-E98B-4CE3-BA77-2F98B7D4BB90.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 320,
        "height" : 400
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 320,
      "height" : 400
    },
    "name" : "Invitation"
  }
]
window.Framer.Defaults.DeviceView = {
  "deviceScale" : -1,
  "orientation" : 0,
  "contentScale" : 1,
  "deviceType" : "apple-watch"
};

window.FramerStudioInfo = {
  "deviceImagesUrl" : "file:\/\/\/Users\/Douglakim06\/Downloads\/Framer%20Studio.app\/Contents\/Resources\/DeviceImages\/"
};

Framer.Device = new Framer.DeviceView();
Framer.Device.setupContext();