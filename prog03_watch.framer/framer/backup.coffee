# This imports all the layers for "PROG 03 Watch" into prog03WatchLayers
prog03WatchLayers = Framer.Importer.load "imported/PROG 03 Watch"

# for l of prog03WatchLayers
# 	print l

Invitation = prog03WatchLayers.Invitation
Invitation.visible = true

Direction = prog03WatchLayers.Direction
Direction.visible = true

Destination_Ahead = prog03WatchLayers.Destination_Ahead
Destination_Ahead.visible = true

Final_Destination = prog03WatchLayers.Final_Destination
Final_Destination.visible = true

acceptButton = prog03WatchLayers.Accept_Button
acceptButton.on Events.Click, ->
	Invitation.animate
		properties:
			x: -320
		time: 0.2
		curve: 'ease_out'
	Direction.animate
		properties:
			x: 0
		time: 0.2
		curve: 'ease_out'

Direction.on Events.Click, ->
	Direction.animate
		properties:
			x: -320
		time: 0.2
		curve: 'ease_out'
	Destination_Ahead.animate
		properties:
			x: 0
		time: 0.2
		curve: 'ease_out'

Destination_Ahead.on Events.Click, ->
	Destination_Ahead.animate
		properties:
			x: -320
		time: 0.2
		curve: 'ease_out'
	Final_Destination.animate
		properties:
			x: 0
		time: 0.2
		curve: 'ease_out'
