# This imports all the layers for "PROG 03" into prog03Layers
prog03Layers = Framer.Importer.load "imported/PROG 03"

# for l of prog03Layers
# 	print l

Start_Screen = prog03Layers.Start_Screen
Start_Screen.visible = true
Start_Screen.backgroundColor = 'white'

Home_Screen = prog03Layers.Home_Screen
Home_Screen.visible = true
Home_Screen.backgroundColor = 'white'

Curious_Screen = prog03Layers.Curious_Screen
Curious_Screen.visible = true
Curious_Screen.backgroundColor = 'white'

Categories = prog03Layers.Categories
Categories.backgroundColor = "white"
Categories.visible = true

Choose_Destination = prog03Layers.Choose_Destination
Choose_Destination.visible = true

Choose_Friends = prog03Layers.Choose_Friends
Choose_Friends.visible = true

Arrival_Time = prog03Layers.Arrival_Time
Arrival_Time.backgroundColor = 'white'
Arrival_Time.visible = true

Sent_Screen = prog03Layers.Sent_Screen
Sent_Screen.backgroundColor = 'white'
Sent_Screen.visible = true

Start_Screen.draggable.enabled = true
Start_Screen.draggable.speedY = 0

Start_Screen.on Events.DragEnd, ->
	if Start_Screen.x < -80
		Start_Screen.animate
			properties:
				x: -640
			time: 0.2
			curve: 'ease_out'
		Home_Screen.animate
			properties:
				x: 0
			time: 0.2
			curve: 'ease_out'
	else
		Start_Screen.animate
			properties:
				x: 0
			time: 0.2
			curve: 'ease_out'

beAdventurous = prog03Layers.Be_Adventurous
beAdventurous.on Events.Click, ->
	Home_Screen.animate
		properties:
			x: -640
		time: 0.2
		curve: 'ease_out'
	Curious_Screen.sendToBack()
		
beCurious = prog03Layers.Be_Curious
beCurious.on Events.Click, ->
	Home_Screen.animate
		properties:
			y: -1136
		time: 0.2
		curve: 'ease_out'
	Curious_Screen.bringToFront()
	Curious_Screen.animate
		properties:
			y: 0
		time: 0.2
		curve: 'ease_out'
	
noLongerCurious = prog03Layers.No_Longer_Curious
noLongerCurious.on Events.Click, ->
	Curious_Screen.animate
		properties:
			y: 1136
		time: 0.2
		curve: 'ease_out'
	Home_Screen.animate
		properties:
			y: 0
		time: 0.2
		curve: 'ease_out'

CategoriesBackLayer = prog03Layers.Categories_Back
CategoriesBackLayer.on Events.Click, ->
	Home_Screen.animate
		properties:
			x: 0
		time: 0.2
		curve: 'ease_out'

restaurantLayer = prog03Layers.Restaurant
restaurantLayer.on Events.Click, ->
	Categories.animate
		properties:
			x:-640
		time:0.2
		curve:'ease_out'

destinationBackLayer = prog03Layers.Location_Back
destinationBackLayer.on Events.Click, ->
	Categories.animate
		properties:
			x:0
		time:0.2
		curve:'ease_out'

healthyHeartsLayer = prog03Layers.Healthy_Hearts
healthyHeartsLayer.on Events.Click, ->
	Choose_Destination.animate
		properties:
			x: -640
		time:0.2
		curve:'ease_out'

friendsBackLayer = prog03Layers.Friends_Back
friendsBackLayer.on Events.Click, ->
	Choose_Destination.animate
		properties:
			x: 0
		time: 0.2
		curve: 'ease_out'

prog03Layers.Green_1.opacity = 0
prog03Layers.Friend_1.on Events.Click, ->
	if prog03Layers.Green_1.opacity == 1
		prog03Layers.Green_1.opacity = 0
		prog03Layers.White_1.opacity = 1
	else
		prog03Layers.Green_1.opacity = 1
		prog03Layers.White_1.opacity = 0

prog03Layers.Green_5.opacity = 0
prog03Layers.Friend_5.on Events.Click, ->
	if prog03Layers.Green_5.opacity == 1
		prog03Layers.Green_5.opacity = 0
		prog03Layers.White_5.opacity = 1
	else
		prog03Layers.Green_5.opacity = 1
		prog03Layers.White_5.opacity = 0

chooseFriendContinue = prog03Layers.Choose_Friend_Continue
chooseFriendContinue.on Events.Click, ->
	Choose_Friends.animate
		properties:
			x: -640
		time: 0.2
		curve: 'ease_out'

timeBackLayer = prog03Layers.Time_Back
timeBackLayer.on Events.Click, ->
	Choose_Friends.animate
		properties:
			x: 0
		time: 0.2
		curve: 'ease_out'

prog03Layers.Scenic_Text.bringToFront()
prog03Layers.Scenic_Green.opacity = 0
prog03Layers.Scenic.on Events.Click, ->
	if prog03Layers.Scenic_Green.opacity == 1
		prog03Layers.Scenic_Green.opacity = 0
		prog03Layers.Scenic_White.opacity = 1
	else
		prog03Layers.Scenic_Green.opacity = 1
		prog03Layers.Scenic_White.opacity = 0

prog03Layers.Touristy_Text.bringToFront()
prog03Layers.Touristy_Green.opacity = 0
prog03Layers.Touristy.on Events.Click, ->
	if prog03Layers.Touristy_Green.opacity == 1
		prog03Layers.Touristy_Green.opacity = 0
		prog03Layers.Touristy_White.opacity = 1
	else
		prog03Layers.Touristy_Green.opacity = 1
		prog03Layers.Touristy_White.opacity = 0

prog03Layers.Exercise_Text.bringToFront()
prog03Layers.Exercise_Green.opacity = 0
prog03Layers.Exercise.on Events.Click, ->
	if prog03Layers.Exercise_Green.opacity == 1
		prog03Layers.Exercise_Green.opacity = 0
		prog03Layers.Exercise_White.opacity = 1
	else
		prog03Layers.Exercise_Green.opacity = 1
		prog03Layers.Exercise_White.opacity = 0

prog03Layers.Food_Text.bringToFront()
prog03Layers.Food_Green.opacity = 0
prog03Layers.Food.on Events.Click, ->
	if prog03Layers.Food_Green.opacity == 1
		prog03Layers.Food_Green.opacity = 0
		prog03Layers.Food_White.opacity = 1
	else
		prog03Layers.Food_Green.opacity = 1
		prog03Layers.Food_White.opacity = 0

arrivalTimeFinish = prog03Layers.Arrival_Time_Finish
arrivalTimeFinish.on Events.Click, ->
	Arrival_Time.animate
		properties:
			x: -640
		time: 0.2
		curve: 'ease_out'

returnHome = prog03Layers.Return_Home
returnHome.on Events.Click, ->
	Sent_Screen.animate
		properties:
			x: -640
		time: 0.2
		curve: 'ease_out'
	Start_Screen.animate
		properties:
			x: 0
		time: 0.2
		curve: 'ease_out'
